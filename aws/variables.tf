#
# Inputs
#

variable "package_filename_lambda" {
  type        = "string"
  description = "path to the zip containing the lambda, poplulated by the ci script"
}

variable "package_keyname_lambda" {
  type        = "string"
  description = "ci generated unique key for the lambda zip file"
}

variable "bucket_of_lambdas" {
  type        = "string"
  description = "existing bucket that holds the lambda.zip files"
  default     = "mergedamagetfs"
}

variable "bucket_namespace" {
  type        = "string"
  description = "key prefix for all items in the bucket"
  default     = "l"
}
