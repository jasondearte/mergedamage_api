# Pre-existing certificate for our domain front end to the lambda
data "aws_acm_certificate" "cert" {
  domain   = "*.${local.base_domain_name}"
  statuses = ["ISSUED"]
}

# Registers a custom domain name for use with AWS API Gateway
resource "aws_api_gateway_domain_name" "site" {
  domain_name     = "${local.app_name}.${local.base_domain_name}"
  certificate_arn = "${data.aws_acm_certificate.cert.arn}"
}

data "aws_route53_zone" "site" {
  name         = "${local.base_domain_name}."
  private_zone = false
  provider     = "aws.global"
}

resource "aws_route53_record" "site" {
  zone_id = "${data.aws_route53_zone.site.id}"
  name    = "${local.app_name}"
  type    = "A"

  alias {
    name                   = "${aws_api_gateway_domain_name.site.cloudfront_domain_name}"
    zone_id                = "${aws_api_gateway_domain_name.site.cloudfront_zone_id}"
    evaluate_target_health = true
  }
}

output "public_url" {
  value = "${aws_api_gateway_domain_name.site.domain_name}"
}

output "public_link" {
  value = "https://${aws_api_gateway_domain_name.site.domain_name}"
}
