#mergedamage_api

Inspired by

https://www.terraform.io/docs/providers/aws/guides/serverless-with-aws-lambda-and-api-gateway.html

# Terraform-docs


## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| bucket_namespace | key prefix for all items in the bucket | string | `l` | no |
| bucket_of_lambdas | existing bucket that holds the lambda.zip files | string | `mergedamagetfs` | no |
| package_filename_lambda | path to the zip containing the lambda, poplulated by the ci script | string | - | yes |
| package_keyname_lambda | ci generated unique key for the lambda zip file | string | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| base_url |  |
| public_link |  |
| public_url |  |

### versions

Terraform v0.11.8

terraform-docs 0.3.0
