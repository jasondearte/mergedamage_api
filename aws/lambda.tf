# Location to keep the lambda zip file(s)
data "aws_s3_bucket" "bucket" {
  bucket = "${var.bucket_of_lambdas}"
}

# Uploads the lambda zip to our s3 bucket
resource "aws_s3_bucket_object" "lambda" {
  bucket = "${data.aws_s3_bucket.bucket.id}"
  key    = "${var.bucket_namespace==""?"":"${var.bucket_namespace}/"}${var.package_keyname_lambda}"
  source = "${var.package_filename_lambda}"
  etag   = "${md5(file("${var.package_filename_lambda}"))}"
}

# Defines the lambda's name, s3 source location, entrypoint and role
resource "aws_lambda_function" "lambda" {
  function_name = "${local.app_name}"
  s3_bucket     = "${aws_s3_bucket_object.lambda.bucket}"
  s3_key        = "${aws_s3_bucket_object.lambda.key}"
  role          = "${aws_iam_role.lambda_exec.arn}"
  handler       = "main.handler"
  runtime       = "nodejs6.10"
}

# Provides an API Gateway REST API
resource "aws_api_gateway_rest_api" "lambda" {
  name        = "${local.app_name}"
  description = "Hello world example"
}

# Create the API Gateway
resource "aws_api_gateway_resource" "proxy" {
  rest_api_id = "${aws_api_gateway_rest_api.lambda.id}"
  parent_id   = "${aws_api_gateway_rest_api.lambda.root_resource_id}"
  path_part   = "{proxy+}"
}

# -------------------------------------------------------------------

# Provides a HTTP Method for an API Gateway Resource
resource "aws_api_gateway_method" "proxy" {
  rest_api_id   = "${aws_api_gateway_rest_api.lambda.id}"
  resource_id   = "${aws_api_gateway_resource.proxy.id}"
  http_method   = "ANY"
  authorization = "NONE"
}

# Provides an HTTP Method Integration for an API Gateway Integration.
resource "aws_api_gateway_integration" "lambda" {
  rest_api_id = "${aws_api_gateway_rest_api.lambda.id}"
  resource_id = "${aws_api_gateway_method.proxy.resource_id}"
  http_method = "${aws_api_gateway_method.proxy.http_method}"
  type        = "AWS_PROXY"
  uri         = "${aws_lambda_function.lambda.invoke_arn}"

  integration_http_method = "POST"
}

# -------------------------------------------------------------------

resource "aws_api_gateway_method" "proxy_root" {
  rest_api_id   = "${aws_api_gateway_rest_api.lambda.id}"
  resource_id   = "${aws_api_gateway_rest_api.lambda.root_resource_id}"
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambda_root" {
  rest_api_id = "${aws_api_gateway_rest_api.lambda.id}"
  resource_id = "${aws_api_gateway_method.proxy_root.resource_id}"
  http_method = "${aws_api_gateway_method.proxy_root.http_method}"
  type        = "AWS_PROXY"
  uri         = "${aws_lambda_function.lambda.invoke_arn}"

  integration_http_method = "POST"
}

# -------------------------------------------------------------------

resource "aws_api_gateway_deployment" "lambda" {
  depends_on = [
    "aws_api_gateway_integration.lambda",
    "aws_api_gateway_integration.lambda_root",
  ]

  rest_api_id = "${aws_api_gateway_rest_api.lambda.id}"
  stage_name  = "test"
}

# Connect {domain_name}/{base_path} -> Lambda
resource "aws_api_gateway_base_path_mapping" "lambda" {
  api_id      = "${aws_api_gateway_rest_api.lambda.id}"
  stage_name  = "${aws_api_gateway_deployment.lambda.stage_name}"
  domain_name = "${aws_api_gateway_domain_name.site.domain_name}"
  base_path   = "test123"
}

# Allow API Gateway to Access Lambda

resource "aws_lambda_permission" "lambda" {
  statement_id  = "AllowAPIGatewayInvoke-lambda"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda.arn}"
  principal     = "apigateway.amazonaws.com"

  # The /*/* portion grants access from any method on any resource
  # within the API Gateway "REST API".
  source_arn = "${aws_api_gateway_deployment.lambda.execution_arn}/*/*"
}

output "base_url" {
  value = "${aws_api_gateway_deployment.lambda.invoke_url}"
}
