'use strict';

var header = "<!DOCTYPE html>\n" +
    "<html lang=\"en\">\n" +
    "<head>\n" +
    "    <meta charset=\"utf-8\">\n" +
    "    <title>MergeDamage</title>\n" +
    "    <meta name=\"description\" content=\"merge damage\">\n" +
    "    <meta name=\"author\" content=\"Jason De Arte\">\n" +
    "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
    "    <link href=\"//fonts.googleapis.com/css?family=Raleway:400,300,600\" rel=\"stylesheet\" type=\"text/css\">\n" +
    "    <link rel=\"stylesheet\" href=\"//mergedamage.com/Skeleton-2.0.4/css/normalize.css\">\n" +
    "    <link rel=\"stylesheet\" href=\"//mergedamage.com/Skeleton-2.0.4/css/skeleton.css\">\n" +
    "</head>\n" +
    "<body>\n\t";

var footer = "\n</body></html>"

exports.handler = function (event, context, callback) {
    var response = {
        statusCode: 200,
        headers: {
            'Content-Type': 'text/html; charset=utf-8',
        },
        body: `${header}<p>Hello world! 3</p>${footer}`,
    };
    callback(null, response);
};
