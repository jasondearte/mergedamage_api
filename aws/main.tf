locals {
  base_domain_name = "mergedamage.com"
  app_name         = "apitest01"
  full_dns         = "${local.app_name}.${local.base_domain_name}"
}
